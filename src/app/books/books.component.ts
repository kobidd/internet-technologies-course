import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import{Observable} from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  panelOpenState = false;
  books:any;
  books$:Observable<any>;
  userId:string;

  deleteBook(id:string){
    this.bookservice.deleteBook(id,this.userId);
  }
  constructor(private bookservice:BooksService,
              public authservice:AuthService) { }

  ngOnInit() {
    /*
    this.books = this.bookservice.getBooks().subscribe(
      (books)=>this.books = books
    )*/
    //this.bookservice.addBook();
    //this.books$ = this.bookservice.getBooks();
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.bookservice.getBooks(this.userId);
      }
    )

  }

}

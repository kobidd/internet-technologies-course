// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
  apiKey: "AIzaSyAa89h_IL_oy2hSaE1LYx1tRzeLoRBKAmY",
  authDomain: "tech-6d9df.firebaseapp.com",
  databaseURL: "https://tech-6d9df.firebaseio.com",
  projectId: "tech-6d9df",
  storageBucket: "tech-6d9df.appspot.com",
  messagingSenderId: "345676977449",
  appId: "1:345676977449:web:bb98d9d1fe3e0e9d6b5338",
  measurementId: "G-TT3X2Q7Q6B"
}
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

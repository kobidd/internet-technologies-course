import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router) {}
  email:string;
  password:string;

  onSubmit(){
    this.authservice.signup(this.email,this.password);
    //this.router.navigate(['/books']);
  }
  ngOnInit() {
  }
}

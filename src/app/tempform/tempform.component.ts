import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {
cityName:string[]=['Non existent','Jerusalem','Paris','London'];
city:string;
temperature:number;

onSubmit(){
  this.router.navigate(['/temperatures', this.temperature,this.city]);
}
  constructor(private router: Router) { }

  ngOnInit() {
  }

}

import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router) { }
  email:string;
  password:string;

  onSubmit(){
    this.authservice.login(this.email,this.password);
    //this.router.navigate(['/books'])

  }

  ngOnInit() {
  }

}
